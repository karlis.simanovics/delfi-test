function renderErrors() {
	for (var i=0; i < errors.length; i++) {
		$("#errors").append("<div> &raquo; " + errors[i] + "</div>").show();
	};
}

var errors = [];

function validateForm() {
	// Reset error array
	errors = [];
	result = true;

	// Clear any existing errors from a previous validation attempt, otherwise they stack
	$("#errors").empty();
	$("#errors").hide();

	// Bad practice to find individual elements by name, what if a new form element is created? A loop is much more effective
	$('input,textarea').filter('[required]').each(function() {
		if ($(this).val().trim() == "") {
			errors.push("Lauks '" + $(this).data("label") + "' ir jānorāda obligāti");
			result = false;
		}
	});

	if (errors[0] == undefined) {
		return true;
	} else {
		renderErrors();
		return false;
	}
}

// activates only when everything is loaded
$(document).ready(function() {
	// activates when the custom button is pressed
	$("#submit").click(function() {
		// Continue to send post request only if no errors are rendered
		if (validateForm()) {
			// Make a post without refreshing the page (shorthand Ajax function)
			$.post( "send_mail.php", $( "#contact-form" ).serialize())
				// validate the server answer
				.done(function(data) {
					// translate the server answer for javascript to understand
					var response = JSON.parse(data);
					// if success is true, the email has been sent succesfully, otherwise errors are displayed
					if (response.success) {
						alert(response.message);
					} else {
						// Loop through error array and push them to array
						$.each(response.errors, function(i, error) {
							errors.push(error);
						});
						renderErrors();
					}
				})
				.fail(function() {
					alert( "Nevarēja pieslēgties failam, mēģiniet vēlreiz." );
				});
		}
	});
});

