<?php
// Load sensitive credential info from a private file, hardcoding credentials in a publicly available file can be trouble
$config = parse_ini_file('../private/config.ini');

// Front-end validation can be bypassed, need to sanitize input fields to check if they are usable
$fullName = filter_input(INPUT_POST, 'full-name', FILTER_SANITIZE_STRING);
$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_NUMBER_INT);
$address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_EMAIL);
$message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

$errors = array();
// Validate all required fields. If a validation fails, make an error message
if (empty($fullName)) {
    array_push($errors, "Laukam 'Vārds, Uzvārds' ir kļūdains saturs, mēģiniet vēlreiz");
}
if (!filter_var($phone, FILTER_VALIDATE_INT)) {
    array_push($errors, "Laukam 'Telfona numurs' ir kļūdains saturs, mēģiniet vēlreiz");
}
if (empty($message)) {
    array_push($errors, "Laukam 'Ziņojums' ir kļūdains saturs, mēģiniet vēlreiz");
}

// If there are errors, send them to front-end
if (!empty($errors)) {
    die(json_encode(array('success' => false, 'errors' => $errors)));
}

// email variables
$subject = "Delfi test e-pasts no " . $fullName ;
$message = "Vārds: " . $fullName . PHP_EOL
            . "Telefona numurs: " . $phone . PHP_EOL
            . "E-pasta adrese: " . $address . PHP_EOL
            . PHP_EOL
            . "Ziņojums: " . $message;
$headers = "From: delfitest@delfi.karlis.id.lv";

// Check if the mail is successfully sent
// @symbol is used to suppress the warning call
if (@mail($config['recipient'], $subject, $message, $headers)) {
    die(json_encode(array('success' => true, 'message' => 'E-pasts veiksmīgi nosūtīts.')));
} else {
    array_push($errors, "Neizdevās pieslēgties E-pastu izsūtīšanas serverim, mēģiniet vēlreiz.");
    die(json_encode(array('success' => false, 'errors' => $errors)));
}


