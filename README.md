## Delfi test assignement

---
## Back-end + javascript logic / html improvements.

1. Created "send_mail.php", back-end server logic for e-mail sending.
2. Created "config.ini", a private configuration file which is meant to store sensitive credentails.
3. Major modifications of **base.js** for use with the new back-end functionality.
4. Optimized code of **index.html**.
---

---
## Front-end modifications / QOL improvements / optimization.

1. Created a folder structure for the project.
2. Updated jquery library version from **v3.0.0** to **v3.4.1**.
3. Added Bootstrap library.
4. Using minified versions of all **js** and **css** files.
5. Optimized code of **index.html**.
6. Optimized code of **base.scss**.
---

---
## Base commit.

1. Base commit, nothing else.
---